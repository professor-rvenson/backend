package com.example.demosemana14.dtos;

public record UserDto(
        String username,
        String email
) {
}
